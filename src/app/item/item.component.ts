import {Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.sass']
})
export class ItemComponent {

  @Input() data: any;
  @Output() action = new EventEmitter<any>();

  addToFav(s, i) {
    this.data[s].items[i].fav = true;
    const item = this.data[s].items[i];
    let items = [];
    if (localStorage.getItem('favorite')) {
      items = JSON.parse(localStorage.getItem('favorite'));
      items.push(item);
    }
    items.push(item);
    localStorage.setItem('favorite', JSON.stringify(items));
  }

  removeFromFav(s, index, id) {
    let items = [];
    if (localStorage.getItem('favorite')) {
      items = JSON.parse(localStorage.getItem('favorite'));
      for (const i in items) {
        if (items[i].imdbID === id) {
          items.splice(Number(i), 1);
        }
      }
    }
    localStorage.setItem('favorite', JSON.stringify(items));
    this.action.emit({action: 'removeFromFav', data: {section: s, index: index}});
  }

}
