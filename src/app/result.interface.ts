export interface IResult {
  Poster: string;
  Title: string;
  Type: string;
  Year: string;
  imdbID: string;
}
