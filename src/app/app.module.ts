import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { SearchComponent } from './search/search.component';
import { ItemComponent } from './item/item.component';
import { FavoriteComponent } from './favorite/favorite.component';
import { InfoComponent } from './info/info.component';
import { DataService } from './app.service';

/* Angular materials */
import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatButtonModule,
  MatCardModule,
  MatFormFieldModule,
  MatGridListModule, MatIconModule,
  MatInputModule, MatPaginatorModule, MatProgressSpinnerModule,
  MatTabsModule, MatToolbarModule
} from '@angular/material';

/* Routes */
const appRoutes: Routes = [
  { path: 'search', component: SearchComponent },
  { path: 'favorite', component: FavoriteComponent },
  { path: 'info/:id', component: InfoComponent },
  { path: '', redirectTo: 'search', pathMatch: 'full' },
  { path: '**', component: SearchComponent }
];


@NgModule({
  declarations: [
    AppComponent,
    ItemComponent,
    SearchComponent,
    FavoriteComponent,
    InfoComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule,
    HttpClientModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatTabsModule,
    MatGridListModule,
    MatBadgeModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatToolbarModule,
    MatPaginatorModule,
    MatAutocompleteModule
  ],
  providers: [
    DataService
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { }
