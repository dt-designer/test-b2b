import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.component.html',
  styleUrls: ['./favorite.component.sass']
})
export class FavoriteComponent implements OnInit {

  constructor() {}

  public resultReady: boolean = false;
  public resultError: string = null;

  public result: any = [
    {
      id: 'movie',
      title: 'Movies',
      items: []
    },
    {
      id: 'series',
      title: 'Series',
      items: []
    },
    {
      id: 'game',
      title: 'Games',
      items: []
    }
  ];

  ngOnInit() {
    let items = localStorage.getItem('favorite');
    if (items) {
      this.resultReady = true;
      items = JSON.parse(items);
      for (const item of items) {
        for (const section of this.result) {
          if (section.id === item['Type']) section.items.push(item);
        }
      }
    } else {
      this.resultReady = false;
      this.resultError = 'You do not have favorite movies';
    }
  }

  itemAction(event) {
    switch (event.action) {
      case 'removeFromFav':
        this.result[event.data.section].items.splice(event.data.index, 1);
        break;
    }
  }

}
