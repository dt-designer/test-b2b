import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../environments/environment';
import { IResult } from './result.interface';

const omdbapi = environment.omdbapi;
const omdbapikey = environment.omdbapikey;

@Injectable()
export class DataService {

  constructor(
    private http: HttpClient,
  ) { }

  search(name, resultPage) {
    return this.http.get<IResult>(omdbapi + '?apikey=' + omdbapikey + '&s=' + name + '&page=' + (resultPage + 1));
  }

  getInfo(id) {
    return this.http.get(omdbapi + '?apikey=' + omdbapikey + '&plot=full&i=' + id);
  }

}
