import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../app.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.sass']
})
export class InfoComponent implements OnInit {

  public itemId: string;

  public resultLoading: boolean = false;
  public resultReady: boolean = false;
  public resultError: string;
  public result: any;

  constructor(
    private route: ActivatedRoute,
    private omdb: DataService
  ) {}

  ngOnInit() {
    this.route.params.subscribe(
      params => {
        this.itemId = params['id']
        this.info();
      }
    );
  }

  info() {
    this.resultLoading = true;
    this.resultReady = false;
    this.omdb.getInfo(this.itemId).subscribe((data) => {
      if (data['Response'] === 'True') {
        console.log(data);
        this.result = data;
        this.resultReady = true;
        this.resultLoading = false;
      } else if (data['Response'] === 'False') {
        this.resultError = '';
      }
    });
  }

}
