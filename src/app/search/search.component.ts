import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { DataService } from '../app.service';
import { IResult } from '../result.interface';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.sass']
})
export class SearchComponent {

  constructor(
    private omdb: DataService
  ) {}

  public autocomleteList: any = [];

  public resultLoading: boolean = false;
  public resultReady: boolean = false;
  public resultTotal: number = 0;
  public resultPage: number = 0;
  public resultCount: number = 10;
  public resultError: string = null;
  public result: any = [
    {
      id: 'movie',
      title: 'Movies',
      items: []
    },
    {
      id: 'series',
      title: 'Series',
      items: []
    },
    {
      id: 'game',
      title: 'Games',
      items: []
    }
  ];

  public searchForm: FormGroup = new FormGroup({
    searchField: new FormControl()
  });

  autocomlete() {
    if (this.searchForm.value.searchField.length >= 3) {
      this.omdb.search(this.searchForm.value.searchField, this.resultPage).subscribe((data) => {
        this.autocomleteList = data['Search'];
      });
    }
  }

  search() {
    this.resultLoading = true;
    this.resultReady = false;
    for (const section of this.result) {
      section.items = [];
    }
    this.omdb.search(this.searchForm.value.searchField, this.resultPage).subscribe((data: IResult) => {
      if (data['Response'] === 'True') {
        let favorites = [];
        if (localStorage.getItem('favorite')) {
          favorites = JSON.parse(localStorage.getItem('favorite'));
        }
        for (const item of data['Search']) {
          item['fav'] = false;
          if (favorites.length) {
            for (let fav of favorites) {
              if (fav.imdbID === item['imdbID']) item['fav'] = true;
            }
          }
          for (const section of this.result) {
            if (section.id === item.Type) section.items.push(item);
          }
        }
        this.resultTotal = data['totalResults'];
        this.resultReady = true;
        this.resultLoading = false;
      } else if (data['Response'] === 'False') {
        this.resultError = '';
      }
    });
  }

  setPage(event) {
    this.resultPage = event.pageIndex;
    this.search();
  }

  itemAction(event) {
    switch (event.action) {
      case 'removeFromFav':
        this.result[event.data.section].items[event.data.index].fav = false;
        break;
    }
  }

}
